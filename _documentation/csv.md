---
title: 'Output: using CSV'
description: Configure Crowl to save data to CSV files.  
layout: documentation
order: 5
toc: true
---
# Exporting data to CSV

CSV is the default output for Crowl.  

## Activate MySQL Pipeline

You can choose which pipeline to run in your project's configuration file.  
As CSV is the default pipeline, you won't have anything to do if you didn't change this setting.  
See [this page](/documentation/configuration/#output-modes) for more details.  


## How it works

When you launch a crawl with CSV pipeline, Crowl will:  
1. Create a URLs file using your project basename (set in the [configuration file](/documentation/configuration/)), and adding a timestamp to it. For instance, your new file could be `foo_20180921-123000_urls.csv`.  
2. Create a links file with the same process. For example: `foo_20180921-123000_links.csv`.  
3. Then Crowl starts crawling and inserting data.  
