---
tag: v0.3
layout: documentation
date: 2021-04-30
---
This new version comes with a few resolved bugs, and some new features. I tried my best to list these below, but I'm quite sure I forgot some ;)  

**New features**
Crawl configuration  
- Exclusion pattern: enables you to exclude some URL patterns from the crawl. [Learn more]().  
- Proxies: you can now provide a list of proxies to be used by the crawler. [Learn more](/documentation/configuration/#use_proxies).  
- Rotating User-agents: you can provide a list of user-agents to be used by the crawler. [Learn more]().  
- Disable referer: remove referer from HTTP request headers. [Learn more]().  

Auth
- New section added to the config files  
- HTTP authentication: simple user/password HTTP authentication. [Learn more]().  

Extraction  
- Limit number of requests. [Learn more]().      
- Remove duplicate links. [Learn more]().    
- Get `x_cache` HTTP header. [Learn more]().    
- Extract complete HTTP headers (both request and response). [Learn more]().    
- Use custom xpath extractors. [Learn more]().    
- Check the language of the content on the page. [Learn more]().  

**Coming soon (hopefully)**
- custom settings    
- include only links  
- list  
- regex & css extractors  

**Also new**
As suggested by some, you can now support Crowl on [Buy me a coffee](https://www.buymeacoffee.com/crowltech)!  
Either with a one-time paiment or by becoming a member, you'll help raise funds for this project. And more funds means more time to add awesome features!  