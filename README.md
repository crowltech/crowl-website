# crowl.tech

Source code and content for <https://www.crowl.tech/>.
Uses Jekyll.
Based on [devAid](https://github.com/xriley/devAid-Theme) theme from [Xiaoying Riley](http://xiaoyingriley.com/).

Plugins:  
- https://github.com/amedia/premonition  


